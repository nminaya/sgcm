﻿using SGCM.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Services
{
    public class MailManager : IMailManager
    {
        private readonly ISmtpClient smtpClient;

        public MailManager(ISmtpClient smtpClient)
        {
            this.smtpClient = smtpClient;
        }

        public Task SendEmail(string to, string subject, string bodyMessage)
        {
            var mail = new MailMessage(AppSettingsHelper.MailSender_Email, to)
            {
                Subject = subject,
                Body = bodyMessage,
                IsBodyHtml = true
            };

            return smtpClient.Send(mail);
        }
    }

    public class SmtpClientWrapper : ISmtpClient
    {
        private SmtpClient SmtpClient
        {
            get => new SmtpClient
            {
                Port = AppSettingsHelper.MailSender_Port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                Host = AppSettingsHelper.MailSender_Host,
                Credentials = new System.Net.NetworkCredential(AppSettingsHelper.MailSender_Email, AppSettingsHelper.MailSender_Password)
            };
        }

        public Task Send(MailMessage mailMessage)
        {
            return SmtpClient.SendMailAsync(mailMessage);
        }
    }
}
