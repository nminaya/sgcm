﻿using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Services.Repository
{
    public interface IUserRepository
    {
        Task<bool> IsValidUser(string email, string password);

        Task<User> GetUser(string email, string password);

        Task RegisterUser(PatientRegistration patient);

        Task<bool> UserExist(string email);
    }
}
