﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCM.Core.Models;
using SGCM.Core.Extensions;

namespace SGCM.Core.Services.Repository
{
    public class ScheduleRepository : RepositoryBase, IScheduleRepository
    {
        public IEnumerable<Schedule> Get(int year, int month, int unitId)
        {
            string query = "SELECT * FROM V_AGENDA WHERE COD_EMPRESA = 1 " +
                "AND ID_SUCURSAL = 1 " +
                $"AND COD_UNIDAD = {unitId} " +
                $"AND ANO_AGENDA = {year} " +
                $"AND MES_AGENDA = {month}";

            var result = ExecuteSelect(query);

            while (result.Read())
            {
                yield return new Schedule
                {
                    CompanyId = int.Parse(result.GetValue(0).ToString()),
                    BranchId = int.Parse(result.GetValue(1).ToString()),
                    UnitId = int.Parse(result.GetValue(2).ToString()),
                    Year = int.Parse(result.GetValue(3).ToString()),
                    Month = int.Parse(result.GetValue(4).ToString()),
                    Sunday = result.GetValue(5).ToString().ToDateTime(),
                    Monday = result.GetValue(6).ToString().ToDateTime(),
                    Tuesday = result.GetValue(7).ToString().ToDateTime(),
                    Wednesday = result.GetValue(8).ToString().ToDateTime(),
                    Thursday = result.GetValue(9).ToString().ToDateTime(),
                    Friday = result.GetValue(10).ToString().ToDateTime(),
                    Saturday = result.GetValue(11).ToString().ToDateTime(),
                    IsWorkingSunday = result.GetValue(11).ToString() == "S",
                    IsWorkingMonday = result.GetValue(12).ToString() == "S",
                    IsWorkingTuesday = result.GetValue(13).ToString() == "S",
                    IsWorkingWednesday = result.GetValue(14).ToString() == "S",
                    IsWorkingThursday = result.GetValue(15).ToString() == "S",
                    IsWorkingFriday = result.GetValue(16).ToString() == "S",
                    IsWorkingSaturday = result.GetValue(17).ToString() == "S",
                };
            }
        }
    }
}
