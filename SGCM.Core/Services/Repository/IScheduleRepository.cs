﻿using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Services.Repository
{
    public interface IScheduleRepository
    {
        IEnumerable<Schedule> Get(int year, int month, int unitId);
    }
}
