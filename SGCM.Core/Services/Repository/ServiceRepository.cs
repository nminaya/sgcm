﻿using SGCM.Core.Models;
using System.Collections.Generic;

namespace SGCM.Core.Services.Repository
{
    public class ServiceRepository : IServiceRepository
    {
        public IEnumerable<Service> GetAll()
        {
            return new[]
            {
                new Service
                {
                    ServiceId = 122,
                    ServiceName = "CONSULTA SEGUIMIENTO"
                },
                new Service
                {
                    ServiceId = 121,
                    ServiceName = "PRIMERA CONSULTA"
                }
            };
        }
    }
}