﻿using Oracle.ManagedDataAccess.Client;
using SGCM.Core.Extensions;
using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace SGCM.Core.Services.Repository
{
    public class ScheduleDayRepository : RepositoryBase, IScheduleDayRepository
    {
        public IEnumerable<ScheduleDay> GetAppointments(int unitId, DateTime date)
        {
            string sql = "SELECT * FROM V_AGENDA_DIA WHERE COD_EMPRESA = 1 " +
                "AND ID_SUCURSAL = 1 " +
                $"AND COD_UNIDAD = {unitId} " +
                $"AND TRUNC(FECHA) = TO_DATE('{date.Date:dd/MM/yyyy}', 'dd/MM/yyyy')";

            var result = ExecuteSelect(sql);

            while (result.Read())
            {
                yield return ConvertToScheduleDay(result);
            }
        }

        public ScheduleDay GetScheduleDay(int unitId, int userId, DateTime date)
        {
            string sql = "SELECT * FROM V_AGENDA_DIA WHERE COD_EMPRESA = 1 " +
                "AND ID_SUCURSAL = 1 " +
                $"AND COD_UNIDAD = {unitId} " +
                $" AND COD_PACIENTE = {userId}" +
                $"AND TRUNC(FECHA) = TO_DATE('{date.Date:dd/MM/yyyy}', 'dd/MM/yyyy')";

            var result = ExecuteSelect(sql);

            while (result.Read())
            {
                return ConvertToScheduleDay(result);
            }

            return null;
        }

        public IEnumerable<ScheduleDay> GetAllDoctorAppointments(int doctorId)
        {
            string sql = "SELECT * FROM V_AGENDA_DIA " +
                $"WHERE COD_DOCTOR = {doctorId}";

            var result = ExecuteSelect(sql);

            while (result.Read())
            {
                yield return ConvertToScheduleDay(result);
            }
        }

        public IEnumerable<ScheduleDay> GetPendingDoctorAppointments(int doctorId)
        {
            return GetAllDoctorAppointments(doctorId)
                .Where(v => v.Date >= DateTime.Now);
        }

        public IEnumerable<ScheduleDay> GetAllPatientAppointments(int patientId)
        {
            string sql = "SELECT * FROM V_AGENDA_DIA " +
                $"WHERE COD_PACIENTE = {patientId}";

            var result = ExecuteSelect(sql);

            while (result.Read())
            {
                yield return ConvertToScheduleDay(result);
            }
        }

        public IEnumerable<ScheduleDay> GetPendingPatientAppointments(int patientId)
        {
            return GetAllPatientAppointments(patientId)
                .Where(v => v.Date >= DateTime.Now);
        }

        public Task CreateAppointment(int userId, int serviceId, int unitId, DateTime date)
        {
            var list = new List<(string name, OracleDbType dbType, object value)>
            {
                ("P_COD_EMPRESA", OracleDbType.Int32, 1),
                ("P_COD_SUCURSAL", OracleDbType.Int32, 1),
                ("P_COD_UNIDAD", OracleDbType.Int32, unitId),
                ("P_FECHA", OracleDbType.Date, date),
                ("P_COD_PACIENTE", OracleDbType.Int32, userId),
                ("P_COD_SERVICIO", OracleDbType.Int32, serviceId)
            };

            return ExecuteCommand("p_agenda_detalle", list, ("erorV", OracleDbType.Varchar2));
        }

        public Task ConfirmAppointment(int userId, int serviceId, int unitId, DateTime date)
        {
            var list = new List<(string name, OracleDbType dbType, object value)>
            {
                ("P_COD_EMPRESA", OracleDbType.Int32, 1),
                ("P_COD_SUCURSAL", OracleDbType.Int32, 1),
                ("P_COD_UNIDAD", OracleDbType.Int32, unitId),
                ("P_FECHA", OracleDbType.Date, date),
                ("P_COD_PACIENTE", OracleDbType.Int32, userId),
                ("P_COD_SERVICIO", OracleDbType.Int32, serviceId)
            };

            return ExecuteCommand("p_confirma_cita", list);
        }

        private ScheduleDay ConvertToScheduleDay(DbDataReader result)
        {
            return new ScheduleDay
            {
                CompanyId = int.Parse(result["COD_EMPRESA"].ToString()),
                BranchId = int.Parse(result["ID_SUCURSAL"].ToString()),
                UnitId = int.Parse(result["COD_UNIDAD"].ToString()),
                Date = result["FECHA"].ToString().ToDateTime(),
                FromDate = result["HORA_DESDE"].ToString().ToDateTime(),
                ToDate = result["HORA_HASTA"].ToString().ToDateTime(),
                PatientId = int.Parse(result["COD_PACIENTE"].ToString()),
                PatientName = result["NOM_PAC"].ToString(),
                PatientPhone = result["TEL_RESIDENCIA"].ToString(),
                ServiceName = result["DES_SERVICIO"].ToString(),
                DoctorId = int.Parse(result["COD_DOCTOR"].ToString()),
                DoctorUnitName = result["CONSULTORIO_DOCTOR"].ToString(),
                IsConfirmed = result["CONFIRMADA"]?.ToString() == "S"
            };
        }
    }
}