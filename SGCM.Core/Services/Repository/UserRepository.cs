﻿using Oracle.ManagedDataAccess.Client;
using SGCM.Core.Enums;
using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SGCM.Core.Services.Repository
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public async Task<User> GetUser(string email, string password)
        {
            string sql = "SELECT * FROM V_USUARIOS " +
                $"WHERE EMAIL = '{email}' " +
                $"AND CLAVE_WEB = '{password}'";

            var result = await ExecuteSelectAsync(sql).ConfigureAwait(false);

            while (await result.ReadAsync().ConfigureAwait(false))
            {
                return new User
                {
                    CompanyId = int.Parse(result["COD_EMPRESA"].ToString()),
                    BranchId = int.Parse(result["ID_SUCURSAL"].ToString()),
                    UserType = $"{result["TIPO_PERSONA"]}" == "PACIENTE" ? UserTypes.Patient : UserTypes.Doctor,
                    UserId = int.Parse(result["COD_PACIENTE"].ToString()),
                    Firstname = result["NOBMRES"].ToString(),
                    Lastname = result["APELLIDOS"].ToString(),
                    Email = result["EMAIL"]?.ToString(),
                    Password = result["CLAVE_WEB"]?.ToString()
                };
            }

            return null;
        }

        public async Task<bool> IsValidUser(string email, string password)
        {
            string sql = "SELECT 1 FROM V_USUARIOS " +
                $"WHERE EMAIL = '{email}' " +
                $"AND CLAVE_WEB = '{password}'";

            var result = await ExecuteSelectAsync(sql).ConfigureAwait(false);

            return result.HasRows;
        }

        public async Task<bool> UserExist(string email)
        {
            string sql = "SELECT 1 FROM V_USUARIOS " +
                $"WHERE EMAIL = '{email}'";

            var result = await ExecuteSelectAsync(sql).ConfigureAwait(false);

            return await result.ReadAsync().ConfigureAwait(false);
        }

        public Task RegisterUser(PatientRegistration patient)
        {
            var list = new List<(string name, OracleDbType dbType, object value)>
            {
                ("P_COD_EMPRESA", OracleDbType.Int32, 1),
                ("P_COD_SUCURSAL", OracleDbType.Int32, 1),
                ("P_PRIMER_NOMBRE", OracleDbType.Varchar2, patient.FirstName),
                ("P_SEGUNDO_NOMBRE", OracleDbType.Varchar2, patient.MiddleName),
                ("P_PRIMER_APELLIDO", OracleDbType.Varchar2, patient.SurName),
                ("P_SEGUNDO_APELLIDO", OracleDbType.Varchar2, patient.SecondSurname),
                ("P_CEDULA", OracleDbType.Varchar2, patient.DocumentId),
                ("P_TELEFONO", OracleDbType.Varchar2, patient.Phone),
                ("P_EMAIL", OracleDbType.Varchar2, patient.Email),
                ("P_CLAVE", OracleDbType.Varchar2, patient.Password),
            };

            return ExecuteCommand("P_CREAR_PACIENTE", list, ("erorV", OracleDbType.Varchar2));
        }
    }
}