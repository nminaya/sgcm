﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Services.Repository
{
    public abstract class RepositoryBase
    {
        private readonly OracleConnection _oracleConnection;
        private bool _isConnectionOpen;

        protected RepositoryBase()
        {
            string oradb = ConfigurationManager.AppSettings["SGCMConnection"];
            _oracleConnection = new OracleConnection(oradb);
        }

        protected Task<DbDataReader> ExecuteSelectAsync(string sql)
        {
            CheckConnectionOpened();

            var cmd = new OracleCommand
            {
                Connection = _oracleConnection,
                CommandText = sql,
                CommandType = CommandType.Text
            };

            return cmd.ExecuteReaderAsync();
        }

        protected DbDataReader ExecuteSelect(string sql)
        {
            CheckConnectionOpened();

            var cmd = new OracleCommand
            {
                Connection = _oracleConnection,
                CommandText = sql,
                CommandType = CommandType.Text
            };

            return cmd.ExecuteReader();
        }

        protected Task<int> ExecuteCommand(string procName, IEnumerable<(string name, OracleDbType dbType, object value)> parameters,
            (string name, OracleDbType dbType)? outParam = null)
        {
            CheckConnectionOpened();

            var cmd = new OracleCommand()
            {
                Connection = _oracleConnection,
                CommandType = CommandType.StoredProcedure,
                CommandText = procName
            };

            foreach (var (name, dbType, value) in parameters)
            {
                cmd.Parameters.Add(name, dbType).Value = value;
            }

            if (outParam.HasValue)
            {
                cmd.Parameters.Add(outParam.Value.name, outParam.Value.dbType, 50);
                cmd.Parameters[outParam.Value.name].Direction = ParameterDirection.Output;
            }

            return cmd.ExecuteNonQueryAsync();
        }

        private void CheckConnectionOpened()
        {
            if (!_isConnectionOpen)
            {
                _oracleConnection.Open();
                _isConnectionOpen = true;
            }
        }
    }
}
