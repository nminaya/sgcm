﻿using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Services.Repository
{
    public interface IScheduleDayRepository
    {
        IEnumerable<ScheduleDay> GetAllDoctorAppointments(int doctorId);
        IEnumerable<ScheduleDay> GetPendingDoctorAppointments(int doctorId);
        IEnumerable<ScheduleDay> GetAllPatientAppointments(int patientId);
        IEnumerable<ScheduleDay> GetPendingPatientAppointments(int patientId);
        IEnumerable<ScheduleDay> GetAppointments(int unitId, DateTime date);
        ScheduleDay GetScheduleDay(int unitId, int userId, DateTime date);
        Task ConfirmAppointment(int userId, int serviceId, int unitId, DateTime date);
        Task CreateAppointment(int userId, int serviceId, int unitId, DateTime date);
    }
}
