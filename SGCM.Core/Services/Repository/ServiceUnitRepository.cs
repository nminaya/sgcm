﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCM.Core.Models;

namespace SGCM.Core.Services.Repository
{
    public class ServiceUnitRepository : RepositoryBase, IServiceUnitRepository
    {
        public IEnumerable<ServiceUnit> GetAll()
        {
            var result = ExecuteSelect("SELECT * FROM V_UNIDAD_SERVICIO");

            while (result.Read())
            {
                yield return new ServiceUnit
                {
                    UnitDetail = result.GetString(0),
                    UnitSchedule = result.GetString(1),
                    UnitId = int.Parse(result.GetString(2))
                };
            }
        }
    }
}
