﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Services
{
    public interface IMailManager
    {
        Task SendEmail(string to, string subject, string bodyMessage);
    }

    public interface ISmtpClient
    {
        Task Send(MailMessage mailMessage);
    }
}
