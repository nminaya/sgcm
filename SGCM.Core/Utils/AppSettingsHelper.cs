﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Utils
{
    /// <summary>
	/// Values from appSettings
	/// </summary>
	public static class AppSettingsHelper
    {
        /// <summary>
        /// SMTP Host
        /// </summary>
        public static string MailSender_Host => System.Configuration.ConfigurationManager.AppSettings["MailSender_Host"];

        /// <summary>
        /// SMTP port
        /// </summary>
        public static int MailSender_Port => int.Parse(System.Configuration.ConfigurationManager.AppSettings["MailSender_Port"]);

        /// <summary>
        /// Email address
        /// </summary>
        public static string MailSender_Email => System.Configuration.ConfigurationManager.AppSettings["MailSender_Email"];

        /// <summary>
        /// Email Password
        /// </summary>
        public static string MailSender_Password => System.Configuration.ConfigurationManager.AppSettings["MailSender_Password"];

        /// <summary>
        /// Server Url
        /// </summary>
        public static string ServerUrl => System.Configuration.ConfigurationManager.AppSettings["ServerUrl"];
    }
}
