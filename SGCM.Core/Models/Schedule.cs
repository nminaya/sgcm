﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Models
{
    public class Schedule
    {
        /// <summary>
        /// COD_EMPRESA
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// ID_SUCURSAL
        /// </summary>
        public int BranchId { get; set; }

        /// <summary>
        /// COD_UNIDAD
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// ANO_AGENDA
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// MES_AGENDA
        /// </summary>
        public int Month { get; set; }

        public DateTime? Sunday { get; set; }
        public DateTime? Monday { get; set; }
        public DateTime? Tuesday { get; set; }
        public DateTime? Wednesday { get; set; }
        public DateTime? Thursday { get; set; }
        public DateTime? Friday { get; set; }
        public DateTime? Saturday { get; set; }

        public bool IsWorkingSunday { get; set; }
        public bool IsWorkingMonday { get; set; }
        public bool IsWorkingTuesday { get; set; }
        public bool IsWorkingWednesday { get; set; }
        public bool IsWorkingThursday { get; set; }
        public bool IsWorkingFriday { get; set; }
        public bool IsWorkingSaturday { get; set; }

    }
}
