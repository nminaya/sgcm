﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Models
{
    public class PatientRegistration
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string  SurName { get; set; }
        public string SecondSurname { get; set; }
        public string DocumentId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
