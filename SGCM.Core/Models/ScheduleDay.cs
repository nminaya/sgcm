﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Models
{
    public class ScheduleDay
    {
        /// <summary>
        /// COD_EMPRESA
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// ID_SUCURSAL
        /// </summary>
        public int BranchId { get; set; }

        /// <summary>
        /// COD_SEXO
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// FECHA
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// HORA_DESDE
        /// </summary>
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// HORA_HASTA
        /// </summary>
        public DateTime? ToDate { get; set; }

        /// <summary>
        /// COD_PACIENTE
        /// </summary>
        public int PatientId { get; set; }

        /// <summary>
        /// NOM_PAC
        /// </summary>
        public string PatientName { get; set; }

        /// <summary>
        /// TEL_RESIDENCIA
        /// </summary>
        public string PatientPhone { get; set; }

        /// <summary>
        /// DES_SERVICIO
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// COD_DOCTOR
        /// </summary>
        public int DoctorId { get; set; }

        /// <summary>
        /// COD_UNIDAD
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// CONSULTORIO_DOCTOR
        /// </summary>
        public string DoctorUnitName { get; set; }

        /// <summary>
        /// CONFIRMADA
        /// </summary>
        public bool IsConfirmed { get; set; }
    }
}
