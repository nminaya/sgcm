﻿using SGCM.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Models
{
    public class User
    {
        /// <summary>
        /// COD_EMPRESA
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// ID_SUCURSAL
        /// </summary>
        public int BranchId { get; set; }

        public UserTypes UserType { get; set; }

        /// <summary>
        /// COD_PACIENTE
        /// </summary>
        public int UserId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public override string ToString() => $"{Firstname} {Lastname}";
    }
}
