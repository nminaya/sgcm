﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Models
{
    public class ServiceUnit
    {
        public string UnitDetail { get; set; }
        public string UnitSchedule { get; set; }
        public int UnitId { get; set; }
    }
}
