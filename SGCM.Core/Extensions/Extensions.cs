﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCM.Core.Extensions
{
    public static class Extensions
    {
        public static DateTime? ToDateTime(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            return DateTime.ParseExact(value, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
        }

        public static T Value<T>(this T? value)
            where T : struct
        {
            return value ?? default(T);
        }

        public static string GetDay(this DateTime? value)
        {
            return value.HasValue ? value.Value.Day.ToString() : "";
        }

        public static IEnumerable<T> GetValues<T>(this Enum @enum)
        {
            return Enum.GetValues(@enum.GetType()).Cast<T>();
        }

        public static string ToReadableDate(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("dd/MM/yyyy") : "";
        }

        public static string ToReadableTime(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToReadableTime() : "";
        }

        public static string ToReadableTime(this DateTime date)
        {
            return date.ToString("hh:mm tt");
        }

        public static string ToReadableString(this bool value)
        {
            return value ? "Sí" : "No";
        }

        public static string GetCssClass(this bool value)
        {
            return value ? "available" : "";
        }
    }
}
