using SGCM.Core.Services;
using SGCM.Core.Services.Repository;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace SGCM.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            // Repositories
            container.RegisterType<IScheduleRepository, ScheduleRepository>();
            container.RegisterType<IServiceUnitRepository, ServiceUnitRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IScheduleDayRepository, ScheduleDayRepository>();
            container.RegisterType<IServiceRepository, ServiceRepository>();

            // Services
            container.RegisterType<IMailManager, MailManager>();
            container.RegisterType<ISmtpClient, SmtpClientWrapper>();
        }
    }
}