﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SGCM.Web.Filters
{
    public class UserLoggedAttribute : AuthorizeAttribute
    {
        private bool hasPermision = true;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            hasPermision = httpContext.Session["user"] != null;

            return hasPermision;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!hasPermision)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { controller = "Login", action = "Index" }));
            }
        }
    }
}