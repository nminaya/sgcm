﻿using SGCM.Core.Models;
using System;
using System.Collections.Generic;

namespace SGCM.Web.Models.ViewModels
{
    public class AppointmentCreateVM
    {
        public IEnumerable<ServiceUnit> ServiceUnits { get; set; }
        public IEnumerable<Months> Months { get; set; }

        public IEnumerable<string> Years
        {
            get
            {
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear; i < currentYear + 2; i++)
                {
                    yield return $"{i}";
                }
            }
        }
    }

    public class CreateAppointmentVM
    {
        public int ServiceId { get; set; }
        public int UnitId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }
}