﻿using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCM.Web.Models.ViewModels
{
    public class AppointmentDetailVM
    {
        public int UnitId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }

    public class AppointmentVM
    {
        public AppointmentDetailVM AppointmentDetail { get; set; }

        public IEnumerable<ScheduleDay> Appointments { get; set; }

        public IEnumerable<Service> Services { get; set; }

        public DateTime Date { get; set; }
    }
}