﻿using SGCM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCM.Web.Models.ViewModels
{
    public class SendConfirmationEmailVM
    {
        public User User { get; set; }
        public DateTime AppointmentDate { get; set; }
        public ScheduleDay ScheduleDay { get; set; }
        public int ServiceId { get; set; }
        public int UnitId { get; set; }
    }
}