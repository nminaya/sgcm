﻿using SGCM.Core.Models;
using SGCM.Core.Services;
using SGCM.Core.Services.Repository;
using SGCM.Web.Controllers.Base;
using SGCM.Web.Filters;
using SGCM.Web.Models;
using SGCM.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SGCM.Web.Controllers
{
    public class AppointmentController : BaseController
    {
        private readonly IServiceUnitRepository _serviceUnitRepository;
        private readonly IScheduleRepository _scheduleRepository;
        private readonly IScheduleDayRepository _scheduleDayRepository;
        private readonly IMailManager _mailManager;
        private readonly IServiceRepository _serviceRepository;

        public AppointmentController(IServiceUnitRepository serviceUnitRepository,
            IScheduleRepository scheduleRepository,
            IScheduleDayRepository scheduleDayRepository,
            IServiceRepository serviceRepository,
            IMailManager mailManager)
        {
            _serviceUnitRepository = serviceUnitRepository;
            _scheduleRepository = scheduleRepository;
            _scheduleDayRepository = scheduleDayRepository;
            _mailManager = mailManager;
            _serviceRepository = serviceRepository;
        }

        [UserLogged]
        public ActionResult Index(bool pendings = true)
        {
            var user = Session["user"] as User;
            ViewBag.Pendings = pendings;

            IEnumerable<ScheduleDay> appointments;

            if (pendings)
                appointments = _scheduleDayRepository.GetPendingPatientAppointments(user.UserId).ToArray();
            else
                appointments = _scheduleDayRepository.GetAllPatientAppointments(user.UserId).ToArray();

            return View(appointments.OrderBy(v=>v.Date));
        }

        [UserLogged]
        public ActionResult Doctor(bool pendings = true)
        {
            var user = Session["user"] as User;
            ViewBag.Pendings = pendings;

            IEnumerable<ScheduleDay> appointments;

            if (pendings)
                appointments = _scheduleDayRepository.GetPendingDoctorAppointments(user.UserId);
            else
                appointments = _scheduleDayRepository.GetAllDoctorAppointments(user.UserId);

            return View(appointments);
        }

        [UserLogged]
        public ActionResult Create()
        {
            var model = new AppointmentCreateVM
            {
                Months = Enum.GetValues(typeof(Months)).Cast<Months>(),
                ServiceUnits = _serviceUnitRepository.GetAll()
            };

            return View(model);
        }

        public async Task<ActionResult> Confirmation(int userId, CreateAppointmentVM appointment)
        {
            try
            {
                var appointmentDate = new DateTime(appointment.Year, appointment.Month, appointment.Day);

                await _scheduleDayRepository.ConfirmAppointment(userId, appointment.ServiceId, appointment.UnitId, appointmentDate);

                ViewBag.Message = "Cita Confirmada";
            }
            catch (Exception e)
            {
                ViewBag.Message = $"Error: {e.Message}";
            }

            return View();

        }

        [HttpPost]
        public PartialViewResult AppointmentDetail(AppointmentDetailVM appointmentDetail)
        {
            var date = new DateTime(appointmentDetail.Year, appointmentDetail.Month, appointmentDetail.Day);

            var appointments = _scheduleDayRepository.GetAppointments(appointmentDetail.UnitId, date);

            var model = new AppointmentVM
            {
                AppointmentDetail = appointmentDetail,
                Date = date,
                Services = _serviceRepository.GetAll(),
                Appointments = appointments
            };

            return PartialView(model);
        }

        public PartialViewResult BuildSchedule(int unitId, int year, int month)
        {
            var result = _scheduleRepository.Get(year, month, unitId);
            return PartialView(result);
        }

        [HttpPost]
        public async Task<JsonResult> CreateAppointment(CreateAppointmentVM appointment)
        {
            try
            {
                var appointmentDate = new DateTime(appointment.Year, appointment.Month, appointment.Day);

                var user = Session["user"] as User;

                await _scheduleDayRepository.CreateAppointment(user.UserId, appointment.ServiceId, appointment.UnitId, appointmentDate);

                var scheduleDayCreated = _scheduleDayRepository.GetScheduleDay(appointment.UnitId, user.UserId, appointmentDate);

                string emailHtml = RenderViewToString("SendConfirmationEmail",
                    new SendConfirmationEmailVM
                    {
                        User = user,
                        AppointmentDate = appointmentDate,
                        ScheduleDay = scheduleDayCreated,
                        ServiceId = appointment.ServiceId,
                        UnitId = appointment.UnitId
                    });

                await _mailManager.SendEmail(user.Email, "SGCM - Confirmación de Cita", emailHtml);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}