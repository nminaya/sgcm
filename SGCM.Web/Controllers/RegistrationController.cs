﻿using SGCM.Core.Models;
using SGCM.Core.Services;
using SGCM.Core.Services.Repository;
using SGCM.Web.Controllers.Base;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SGCM.Web.Controllers
{
    public class RegistrationController : BaseController
    {
        private readonly IUserRepository _userRepository;
        private readonly IMailManager _mailManager;

        public RegistrationController(IUserRepository userRepository, IMailManager mailManager)
        {
            _userRepository = userRepository;
            _mailManager = mailManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> RegisterUser(PatientRegistration patient)
        {
            try
            {
                if (await _userRepository.UserExist(patient.Email))
                {
                    throw new Exception("Este correo ya está registrado.");
                }

                await _userRepository.RegisterUser(patient);

                string bodyHtml = RenderViewToString("WelcomeEmail", patient.Email);

                await _mailManager.SendEmail(patient.Email, "SGCM - Bienvenida", bodyHtml);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}