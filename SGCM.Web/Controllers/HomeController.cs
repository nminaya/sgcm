﻿using SGCM.Core.Services;
using SGCM.Core.Services.Repository;
using SGCM.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SGCM.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IServiceUnitRepository _serviceUnitRepository;
        private readonly IMailManager _mailManager;

        public HomeController(IServiceUnitRepository serviceUnitRepository,
            IMailManager mailManager)
        {
            _serviceUnitRepository = serviceUnitRepository;
            _mailManager = mailManager;
        }

        public async Task<ActionResult> Index()
        {
            //string html = Helper.RenderViewToString("Appointment", "SendConfirmationEmail", null);

            //await _mailManager.SendEmail("dj.neon@live.com", "Confirmar Cita (Test)", html);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}