﻿using SGCM.Core.Services.Repository;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SGCM.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserRepository _userRepository;

        public LoginController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> ValidateUser(string email, string password)
        {
            var user = await _userRepository.GetUser(email, password);

            if (user != null)
            {
                Session["user"] = user;
                Session["userType"] = user.UserType;

                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json("Email/Contraseña incorrecta.", JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogOut()
        {
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }
    }
}