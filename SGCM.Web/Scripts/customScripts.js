﻿function loadButtonEffect(selector) {
    var jSelector = "";
    if (typeof selector === "undefined") {
        jSelector = "button";
    }
    else {
        jSelector = selector;
    }

    //Loading Effect
    $(jSelector).on("click", function () {
        var ajaxActivo = $.active;
        var originalHTML = $(this).html();
        var element = this;

        $(element).html("<i class='fa fa-refresh fa-spin'></i>");

        if (ajaxActivo) {
            $(document).ajaxStop(function () {
                $(element).html(originalHTML);
            });
        }
        else {
            $(element).html(originalHTML);
        }
    });
}